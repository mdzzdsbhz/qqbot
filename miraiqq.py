import requests
import time
from stardust.pipe_fn import e
from core.data import GroupReply, PrivateReply, DatabaseManager, QQMessage, Reply

from core.main import Pure
from core import message
import re
from multiprocessing import Process


def POST(url, data=dict()):
    r = requests.post(url, json=data)
    print(r.text)


def GET(url):
    r = requests.get(url)
    print(r.text)

def handlePure(api, cb, user, dbm, msg, res):
    result = cb(user, dbm, msg, res)
    api.handleResult(msg, result)

class MiRaiApi:
    def __init__(self, url, qq, handler):
        self.url = url
        self.handler = handler
        self.qq = qq

        self.post = lambda u, d: requests.post(self.url + u, json=d)
        self.get = lambda u: requests.get(self.url + u)
        self.async_post = lambda u, d: Process(
            target=POST, args=(self.url + u, d)
        ).start()
        self.async_get = lambda u: Process(target=GET, args=(self.url + u)).start()

        self.onstart = []
        self.stopped = False

    def getHandler(self):
        return self.handler

    def sendUserMsg(self, uid, msg, gid=None):
        print(f"[-> u{uid}] {msg}")
        if self.friendList is None:
            self.friendList = (
                self.get(f"/friendList?sessionKey={self.session}").json()
                | e / map @ (lambda x: int(x["id"]))
                | e / list
            )
            self.friendListTimestamp = int(time.time())
        if isinstance(msg, message.MessageChain):
            m = msg
        else:
            m = message.MessageChain([message.PlainText(text=str(msg))])
        if int(uid) in self.friendList:
            k = self.async_post(
                "/sendFriendMessage",
                {"sessionKey": self.session, "qq": int(uid), "messageChain": m.dict(),},
            )
        elif uid in self.groupMemberCache:
            k = self.async_post(
                "/sendTempMessage",
                {
                    "sessionKey": self.session,
                    "qq": int(uid),
                    "group": int(self.groupMemberCache[int(uid)]),
                    "messageChain": m.dict(),
                },
            )
        elif gid is not None:
            k = self.async_post(
                "/sendTempMessage",
                {
                    "sessionKey": self.session,
                    "qq": int(uid),
                    "group": int(gid),
                    "messageChain": m.dict(),
                },
            )
        else:
            print("Invalid Receiver", uid, msg)
            return

    def sendGroupMsg(self, gid, msg):
        print(f"[-> g{gid}] {msg}")
        if isinstance(msg, message.MessageChain):
            m = msg
        else:
            m = message.MessageChain([message.PlainText(text=str(msg))])

        r = self.async_post(
            "/sendGroupMessage",
            {"sessionKey": self.session, "group": int(gid), "messageChain": m.dict(),},
        )
        pass

    def handleResult(self, msg, result):
        assert type(result) is list
        for i in result:
            if isinstance(i, str):
                if msg.msgType == "GroupMessage":
                    self.sendGroupMsg(msg.group_id, i)
                else:
                    self.sendUserMsg(msg.sender_id, i, msg.group_id)
            elif isinstance(i, Reply):
                if isinstance(i, GroupReply):
                    self.sendGroupMsg(msg.group_id, i.msg)
                elif isinstance(i, PrivateReply):
                    self.sendUserMsg(i.user_id, i.msg, msg.group_id)
                else:
                    if msg.msgType == "GroupMessage":
                        self.sendGroupMsg(msg.group_id, i.msg)
                    else:
                        self.sendUserMsg(msg.sender_id, i.msg, msg.group_id)

    def handle(self, msg):
        if msg.msgType == "GroupMessage":
            print(f"[<- g{msg.group_id}] {str(msg.content)}")
            pass
        else:
            print(f"[<- u{msg.sender_id}] {str(msg.content)}")
            pass

        dbm = DatabaseManager()

        user = dbm.getUser(msg.sender_id)
        nickname = user["nickname"] or msg.sender
        user["usercard"] = msg.sender
        todo = self.handler.handle(msg)
        syncTodo = []

        for cb, res in todo:
            if isinstance(cb, Pure):
                Process(target=handlePure, args=(self, cb, user, dbm, msg, res)).start()
            else:
                syncTodo.append((cb, res))

        for cb, res in syncTodo:
            self.handleResult(msg, cb(user, dbm, msg, res))

    def __del__(self):
        self.post("/release", {"sessionKey": self.session, "qq": self.qq})

    def start(self):
        from schedtask.main import s

        self.session = self.post(
            "/auth", {"authKey": "ngkjsdntiosengoiwejetk3w90jtg"}
        ).json()["session"]
        self.post("/verify", {"sessionKey": self.session, "qq": self.qq})

        self.friendList = (
            self.get(f"/friendList?sessionKey={self.session}").json()
            | e / map @ (lambda x: int(x["id"]))
            | e / list
        )
        self.friendListTimestamp = int(time.time())
        self.groupMemberCache = {}

        for i in self.onstart:
            i()

        while True:
            msgs = self.get(f"/fetchMessage?sessionKey={self.session}&count=100").json()
            if msgs["data"].__len__() == 0:
                time.sleep(0.1)
                continue
            print("Received", msgs["data"].__len__())
            for data in msgs["data"]:
                print(data)
                if data["type"] not in ["GroupMessage", "TempMessage", "FriendMessage"]:
                    continue
                source = data["messageChain"].pop(0)
                d = {
                    "raw": data,
                    "time": source["time"],
                    "sender_id": data["sender"]["id"],
                    "sender": None,
                    "receiver_id": self.qq,
                    "receiver": None,
                    "group_id": None,
                    "group": None,
                    "content": data["messageChain"]
                    | e
                    / map
                    @ (
                        lambda x: message.PlainText(x["text"]) if x["type"] == "Plain"
                        else message.Image(imageId=x["imageId"], url=x["url"]) if x["type"] == "Image"
                        else message.App(content=x["content"]) if x["type"] == "App"
                        else None
                    )
                    | e / filter @ (lambda x: x is not None)
                    | e / message.MessageChain,
                    "msgType": data["type"],
                    "at": False,
                }
                if not d["content"]:
                    # print(d)
                    continue
                if data["type"] == "FriendMessage":
                    d["sender"] = data["sender"]["nickname"]
                if data["type"] == "GroupMessage" or data["type"] == "TempMessage":
                    d["sender"] = data["sender"]["memberName"]
                    d["group_id"] = data["sender"]["group"]["id"]
                    self.groupMemberCache[int(data["sender"]["id"])] = int(
                        data["sender"]["group"]["id"]
                    )
                    if (
                        data["messageChain"]
                        | e
                        / filter
                        @ (lambda x: x["type"] == "At" and x["target"] == int(self.qq))
                        | e / list
                        | e / len
                    ) >= 1:
                        d["at"] = True
                msg = QQMessage(d)
                self.handle(msg)

            s.run(blocking=False)
        pass
