import json
from typing import Union
from .attrs import Attr
from core.data import UserDAO
from stardust import asProperty, proxyOn
from stardust.pipe_fn import e


class Card:
    class AttrsProp:
        def __init__(self, card):
            self.card = card
            proxyOn(self, card)

        def __getitem__(self, attrid):
            attrid = Attr.asId(attrid)
            return self._attrs[attrid] if attrid in self._attrs else None

        def __setitem__(self, attrid, value):
            attrid = Attr.asId(attrid)
            self._attrs[attrid] = int(value)
            if self.db:
                self.db.set("attrs", self._attrs)

        def __contains__(self, attrid):
            return self[attrid] is not None

        def __iter__(self):
            return iter(self._attrs.items())

        def __getattr__(self, k):
            try:
                return self[k]
            except KeyError:
                raise AttributeError(k)

    def __init__(self, cardid, name):
        self.cardid = cardid
        self.db = None
        self._attrs = {}
        self._attrsProp = Card.AttrsProp(self)

        asProperty(self, "nickname", default=name, setter=Card.rename)

    @property
    def attrs(self):
        return self._attrsProp

    @staticmethod
    def load(cardid, db):
        card = Card(cardid, db.get("name"))
        card._attrs = db.get("attrs")
        card.bind(db)
        return card

    def bind(self, db):
        db.set("attrs", self._attrs)
        db.set("name", self.nickname)
        self.db = db

    def rename(self, name):
        self._nickname = name
        if self.db:
            self.db.set("name", self.nickname)

    def __eq__(self, other):
        return isinstance(other, Card) and other.cardid == self.cardid

    def __hash__(self):
        return hash(self.cardid)


class UserCard(Card):
    class AttrsProp:
        def __init__(self, card):
            setattr(self, "card", card)
            proxyOn(self, card)

        def __getitem__(self, attrid):
            attrid_key = "trpg.card.attrs." + Attr.asId(attrid)
            return self.pl[attrid_key] if attrid_key in self.pl else None

        def __setitem__(self, attrid, value):
            attrid = Attr.asId(attrid)
            self.pl["trpg.card.attrs." + attrid] = int(value)

        def __contains__(self, attrid):
            return self[attrid] is not None

        def __iter__(self):
            u = lambda f: lambda args: f(*args)

            ret = (
                list(Attr.names().items())
                | e / map @ (u(lambda k, v: (k, self.attrs[k])))
                | e / filter @ (u(lambda k, v: v is not None))
                | e / dict
            )

            return iter(ret.items())

        def __getattr__(self, k):
            try:
                return self[k]
            except KeyError:
                super().__getattr__(k, v)

        def __setattr__(self, k, v):
            try:
                self[k] = v
            except KeyError:
                super().__setattr__(k, v)

    def __init__(self, pl):
        self.pl = pl
        self.cardid = pl["uid"] + ".0"
        self._attrsProp = UserCard.AttrsProp(self)

    @property
    def nickname(self):
        return self.pl["nickname"] or self.pl["usercard"]

    @nickname.setter
    def nickname(self, name):
        self.pl["nickname"] = name

    @property
    def attrs(self):
        return self._attrsProp



def getCard(user: UserDAO) -> Card:
    if "@trpg.card" not in user:
        return UserCard(user)
    else:
        return user["@trpg.card"]
