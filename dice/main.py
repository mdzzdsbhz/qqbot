from stardust import POST, POST_data, fnv32a, dispatch
from stardust.pipe_fn import e
from numpy import random, vectorize, array, ndarray
import datetime
import time
import re
import secrets
from .cards import Card, UserCard, getCard
from .attrs import requiredAttrCheck, Attr
from core.data import Reply, PrivateReply
import config

def genQueue(n=10):
    import json
    from stardust.pipe_fn import e

    data = (
        {
            "jsonrpc": "2.0",
            "method": "generateIntegers",
            "params": {
                "apiKey": config.randomorg_apikey,
                "n": n * 2,
                "min": 0,
                "max": 0xFFFF,
            },
            "id": 0,
        }
        | e / json.dumps
    ).encode("utf-8")
    req = POST_data(
        "https://api.random.org/json-rpc/2/invoke",
        headers={
            "Content-Type": "application/json; charset=utf-8",
            "Content-Length": len(data),
        },
        data=data,
    )
    result = req.read().decode("unicode-escape").replace("\r\n", "")
    s = json.loads(result, strict=False)["result"]["random"]["data"]
    ret = list(map(lambda x: ((x[0] << 16) + x[1]), zip(s[0:n], s[n:])))
    return ret


def coc7(user, dbm, msg, args):
    nickname = getCard(user).nickname
    num = min(5, max(1, int(args["num"] or 1)))

    def roll():
        str = sum(roll_ndn("3d6")) * 5
        con = sum(roll_ndn("3d6")) * 5
        dex = sum(roll_ndn("3d6")) * 5
        app = sum(roll_ndn("3d6")) * 5
        pow = sum(roll_ndn("3d6")) * 5
        luk = sum(roll_ndn("3d6")) * 5

        siz = (sum(roll_ndn("2d6")) + 6) * 5
        edu = (sum(roll_ndn("2d6")) + 6) * 5
        int = (sum(roll_ndn("2d6")) + 6) * 5

        return (
            f"力量 {str} 敏捷 {dex} 体质 {con} 外表 {app} 意志 {pow} 智力 {int} 体型 {siz} 教育 {edu} 幸运 {luk} "
            + f"合計 {str + dex + con + app + pow + int + siz + edu + luk}"
        )

    return [f" * {nickname} 投掷COC 7版 属性 :\n" + "\n".join(roll() for i in range(num))]


def newSeed(dbm):
    now = datetime.datetime.now()
    seedTime = dbm.get("dice").get("@seedTime", None)
    seedQueue = dbm.get("dice").get("@seedQueue", [])

    if seedTime is None or seedTime != (now.hour, now.minute // 12) or not seedQueue:
        dbm.get("dice").set("@seedTime", (now.hour, now.minute // 12))
        dbm.get("dice").set("@seedQueue", genQueue(32))

    q = dbm.get("dice").get("@seedQueue")
    s = q.pop() ^ secrets.randbits(32)
    return s


def renewSeed(dbm):
    random.seed(newSeed(dbm))


def rhd(user, dbm, msg, args):
    nickname = getCard(user).nickname

    return [
        Reply([f" * {nickname} 投了一把隐形骰子 你们这些笨蛋是看不见的"]),
        PrivateReply(msg.sender_id, nrd(user, dbm, msg, args)),
    ]


def nrd(user, dbm, msg, args):
    card = getCard(user)
    nickname = card.nickname

    renewSeed(dbm)

    dice = re.split("([+\-*×/÷])", args["dice"])
    content = args["content"] or ""

    normalized_dice = []
    for i in dice:
        if i in "+-*/":
            normalized_dice.append(i)
        elif i == "×":
            normalized_dice.append("*")
        elif i == "÷":
            normalized_dice.append("/")
        else:
            n, d = rd_normalize(i)
            normalized_dice.append(d or n)

    def f(x):
        if isinstance(x, int):
            return x
        elif x in "+-*/":
            return x
        else:
            d = roll_ndn(x)
            if len(d) == 1:
                return int(d[0])
            else:
                return d

    dice_result = list(map(f, normalized_dice))
    nice_dice = "".join(str(x) for x in normalized_dice)
    nice_dice_result = "".join(str(x) for x in dice_result)
    dice_sum = eval(
        "".join(str(sum(i) if isinstance(i, ndarray) else i) for i in dice_result)
    )

    if len(normalized_dice) == 1 and (
        isinstance(dice_result[0], int) or len(dice_result[0]) == 1
    ):
        return [f" * {nickname} 投擲 {content}: {nice_dice} = {dice_sum}"]
    else:
        return [
            f" * {nickname} 投擲 {content}: {nice_dice} = {nice_dice_result} = {dice_sum}"
        ]
    pass


def ra(user, dbm, msg, args):
    if not args["sname"] and not args["svalue"]:
        return []
    card = getCard(user) or user.getName()

    sname = args["sname"]
    if sname and args["svalue"] == "":
        sid = Attr.asId(sname)
        sname = Attr.getName(sid)
        requiredAttrCheck(card, sid)
        svalue = card.attrs[sid]
    else:
        svalue = int(args["svalue"])
    nickname = card.nickname
    renewSeed(dbm)

    from stardust import when, otherwise, value, case

    dResult = roll_ndn("1d100")[0]
    # dResult = 10

    def separate1d100(dResult):
        if dResult == 100:
            return (0, 0)
        else:
            return (dResult // 10, dResult % 10)

    def combine2d10(ten, one):
        if ten == 0 and one == 0:
            return 100
        else:
            return ten * 10 + one

    def getResult(dResult):
        return when(
            lambda s: s
            | case(lambda: dResult <= max(1, int(svalue / 5))) >> value("極難成功")
            | case(lambda: dResult <= max(1, int(svalue / 2))) >> value("困難成功")
            | case(lambda: dResult <= max(1, int(svalue))) >> value("成功")
            | otherwise >> value("失敗")
        )

    ret = f" * {nickname} {sname}檢定: {dResult}/{svalue} {getResult(dResult)}"

    bonus = args["bonus"]

    if bonus:
        bonus_type = bonus[0]
        bonus_num = bonus[1:]
        if int(bonus_num) != 0:
            bonus_result = list(roll_ndn(f"{bonus_num}d10") - 1)
            separatedResult = separate1d100(dResult)
            if bonus_type == "+":
                adjResult = min(
                    combine2d10(i, separatedResult[1])
                    for i in bonus_result + [separatedResult[0]]
                )
                ret = (
                    f" * {nickname} {sname}檢定: {dResult} -> {adjResult}/{svalue} {getResult(adjResult)} \n"
                    + f" * 獎勵骰: {bonus_result}"
                )
            elif bonus_type == "-":
                adjResult = max(
                    combine2d10(i, separatedResult[1])
                    for i in bonus_result + [separatedResult[0]]
                )
                ret = (
                    f" * {nickname} {sname}檢定: {dResult} -> {adjResult}/{svalue} {getResult(adjResult)} \n"
                    + f" * 懲罰骰: {bonus_result}"
                )

    return [ret]


def rd_normalize(cmd):
    # rndn / rdn / ndn / d / num -> num | ndn
    try:
        return (int(cmd), None)
    except Exception:
        pass
    res = re.match(r"r?(?P<dice_num>\d+)?d(?P<dice_size>\d+)?", cmd, re.IGNORECASE)
    dice_num = res["dice_num"] or "1"
    dice_size = res["dice_size"] or "100"
    return (None, dice_num + "d" + dice_size)


def roll_ndn(cmd):
    result = re.match(r"(\d*)d(\d*)", cmd)
    diceNum = int(result[1] or 1)
    diceSize = int(result[2] or 100)
    ret = random.randint(1, diceSize + 1, diceNum)
    return ret


def jrrp(user, dbm, msg, args):
    now = datetime.datetime.now()
    todayDate = str(now.year) + str(now.month).zfill(2) + str(now.day).zfill(2)

    date, seed = dbm.get("dice").get("jrrpSeed", [None, None])
    if seed is None or date != todayDate:
        seed = newSeed(dbm)
        dbm.get("dice").set("jrrpSeed", (todayDate, seed))

    x = fnv32a(f"add some salt and sugar: {user['uid']} jrrp on {todayDate}")
    state = random.RandomState(seed ^ x)

    rp = state.randint(0, 100)

    nickname = user.getName()

    if rp < 5:
        return [f"* {nickname} 今天的運勢...這麼悲傷的事情..不忍心說啊, 建議你還是別抽卡扭蛋大建打麻雀甚麼的了"]

    return [f"* {nickname} 今天的運勢是 {rp}% ! " + ("|" * int(rp))]


def newCard(user, dbm, msg, args):
    count = user.get("trpg.cardNum", 0)

    cardid = f"{msg.sender_id}.{count + 1}"
    card = Card(cardid, args["name"])
    card_db = dbm.get(f"Card.{cardid}")
    card.bind(card_db)

    user.set("trpg.cardNum", count + 1)
    return [f"* 已創建角色卡 {card.nickname} id:{count + 1}"]


def selectCard(user, dbm, msg, args):
    if args["id"] == "0":
        user.remove("@trpg.card")
        card = getCard(user)
        return [f"* 已選擇角色卡 {card.nickname}"]

    cardid = f"{msg.sender_id}.{args['id']}"
    if not dbm.isExist(f"Card.{cardid}"):
        return [f"* 角色卡不存在"]

    card = Card.load(cardid, dbm.get(f"Card.{cardid}"))
    user.set("@trpg.card", card)

    return [f"* 已選擇角色卡 {card.nickname}"]


def lsCard(user, dbm, msg, args):
    count = user.get("trpg.cardNum", 0)
    if count == 0:
        return ["* 你沒有任何角色卡"]
    toCardId = lambda x: (x, f"{msg.sender_id}.{x}")
    toCard = lambda x: (x[0], Card.load(x, dbm.get(f"Card.{x[1]}")))

    ret = (
        list(range(1, count + 1))
        | e / map @ (toCardId)
        | e / map @ (toCard)
        | e / map @ (lambda x: f"{x[0]}: {x[1].nickname}")
        | e / "\n".join
    )

    return [ret]


def rencard(user, dbm, msg, args):
    cardid = f"{msg.sender_id}.{args['id']}"
    return []


def cardinfo(user, dbm, msg, args):
    if args["id"]:
        if args["id"] == "0":
            card = UserCard(user)
        else:
            cardid = f"{msg.sender_id}.{args['id']}"
            if not dbm.isExist(f"Card.{cardid}"):
                return [f"* 角色卡不存在"]
            card = Card.load(cardid, dbm.get(f"Card.{cardid}"))
    else:
        card = getCard(user)

    if type(card) is UserCard:
        cardid = "0"
    elif args["id"]:
        cardid = args["id"]
    else:
        cardid = card.cardid.split(".")[1]

    hpbar = ""
    if "hp" in card.attrs and "maxhp" in card.attrs:
        hpbar = (
            f"HP [{('*' * int((card.attrs['hp'] / card.attrs['maxhp']) * 10)).ljust(10, '-')}] "
            + f"{card.attrs['hp']}/{card.attrs['maxhp']}\n"
        )

    mpbar = ""
    if "mp" in card.attrs and "maxmp" in card.attrs:
        mpbar = (
            f"MP [{('*' * int((card.attrs['mp'] / card.attrs['maxmp']) * 10)).ljust(10, '-')}] "
            + f"{card.attrs['mp']}/{card.attrs['maxmp']}\n"
        )
    return [
        f"角色卡 {cardid} {card.nickname}\n"
        + hpbar
        + mpbar
        + ",\n".join(
            f"{Attr.getName(k)}({k}): {v}"
            for k, v in card.attrs
            if k not in ("hp", "mp", "maxhp", "maxmp")
        )
    ]


def setSkill(user, dbm, msg, args):
    card: Card = getCard(user)

    def parse(cmd):
        return list(zip(*(iter(cmd.split(" ")),) * 2))

    ret = []
    for sname, svalue in parse(args[1]):
        card.attrs[sname] = svalue
        ret.append(f" * {card.nickname} 的 {Attr.getName(sname)} 已設置為 {svalue}")

    return ret


def sanCheck(user, dbm, msg, args):
    card: Card = getCard(user)

    succ, fail = args["succ"] or "1", args["fail"] or "1d3"

    requiredAttrCheck(card, "san", "pwr")

    pwr = card.attrs["pwr"]

    renewSeed(dbm)
    val = roll_ndn("1d100")[0]
    passed = val <= pwr
    sanAdjRaw = succ if passed else fail

    try:
        sanAdj = int(sanAdjRaw)
    except ValueError:
        sanAdj = roll_ndn(sanAdjRaw).tolist() | e / sum

    card.attrs["san"] -= sanAdj

    sym = "<=" if passed else ">"
    result = "成功" if passed else "失敗"
    san = card.attrs["san"]
    return [
        f" * sc {succ}/{fail}\n"
        f"{card.nickname} 意志檢定結果為 {val} {sym} {pwr} "
        f"判定{result}\n"
        f"失去 {sanAdj} 點理智 當前剩余 {san} 點"
    ]


def reset(user, dbm, msg, args):
    return []


def init(api, handler, prefix=None, subfix=None):
    cmd = [
        (
            r"r(?P<dice>(?:\d*d\d*|\d+)(?:[+\-*/](?:\d*d\d*|\d*))*)(?:\s?(?P<content>.*))?",
            nrd,
        ),
        (
            r"rh(?P<dice>(?:\d*d\d*|\d+)(?:[+\-*/](?:\d*d\d*|\d*))*)(?:\s?(?P<content>.*))?",
            rhd,
        ),
        (r"ra(?P<bonus>[+\-]\d+)?\s?(?P<sname>[^\s]*)\s?(?P<svalue>.*)", ra),
        (r"ss\s?((?:.+ \d+)+)", setSkill),
        (r"sc(?:\s?(?P<succ>\d*d\d+|\d+)/(?P<fail>\d*d\d+|\d+))?", sanCheck),
        (r"(?:今日人品|jrrp).*", jrrp),
        (r"newcard (?P<name>.*)", newCard),
        (r"selcard (?P<id>.*)", selectCard),
        (r"lscard", lsCard),
        (r"rencard (?P<id>\d*)\s?(?P<name>.+)", rencard),
        (r"cardinfo\s?(?P<id>.*)", cardinfo),
        (r"coc7\s?(?P<num>\d*)", coc7),
    ]
    for i in cmd:
        handler.register(i[0], i[1], prefix=prefix, subfix=subfix)
