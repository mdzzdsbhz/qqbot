from urllib.parse import urlencode
from urllib.request import Request, urlopen
import json


def POST(url, headers=dict(), data=dict()):
   request = Request(url, urlencode(data).encode())
   for k in headers:
      request.add_header(k, headers[k])
   return urlopen(request)


def GET(url, headers=dict()):
   request = Request(url)
   for k in headers:
      request.add_header(k, headers[k])
   return urlopen(request)


def GET_json(url, headers=dict()):
   return json.loads(
       GET(url, headers).read().decode("unicode-escape").replace('\r\n', ''), strict=False)


def POST_json(url, headers=dict(), data=dict()):
   return json.loads(
       POST(url, headers, data).read().decode("unicode-escape").replace('\r\n', ''), strict=False)


# ==
def djb2(string):
   hashCode = 5381
   for i in string:
      hashCode = ((hashCode << 5) + hashCode) + ord(i)
   return hashCode


def fnv32a(str):
   hashCode = 0x811c9dc5
   fnv_32_prime = 0x01000193
   uint32_max = 2**32
   for s in str:
      hashCode ^= ord(s)
      hashCode *= fnv_32_prime
   return hashCode & (uint32_max - 1)


# ==
"""in-place"""


def clearDictNone(d):
   for k, v in list(d.items()):
      if not v:
         del d[k]
   return d


class Bunch(object):
  def __init__(self, adict):
    self.__dict__.update(adict)
