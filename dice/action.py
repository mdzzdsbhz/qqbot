from .main import ra
from .attrs import Attr
def do(user, dbm, msg, args):
   try:
      Attr.asId(args['sname'])
      return ra(user, dbm, msg, args)
   except KeyError:
      return []

def init(api, handler, prefix=None, subfix=None):
   cmd = [
      (r"(?P<sname>[^\s+\-]*)(?P<bonus>[+\-]\d+)?\s?(?P<svalue>.*)", do),
   ]
   for i in cmd:
      handler.register(i[0], i[1], prefix=prefix, subfix=subfix)
