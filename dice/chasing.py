from .cards import getCard, Card
from .attrs import Attr, requiredAttrCheck
from typing import List, Dict, Any
from itertools import groupby
from stardust import asProperty
from core.data import Reply

from stardust.pipe_fn import e


def getScene(dbm):
    db = dbm.get("trpg.chasing")
    s = db.get("@scene")
    if not s:
        raise Reply(["* 當前不存在追逐戰"])
    return s


class ChaseScene:
    class CardInfo:
        def __init__(self, card, loc=0, isBoss=False):
            self.card = card

            asProperty(self, "maxActPt", setter=None, default=0)
            asProperty(self, "currActPt", setter=None, default=0)
            asProperty(self, "loc", setter=None, default=loc)
            asProperty(self, "isBoss", setter=None, default=isBoss)

        def __getattr__(self, attr):
            return getattr(self.card, attr)

    def __init__(self):
        self._cards: Dict[Card, CardInfo] = {}
        self.boss: List[Card] = []
        self.round = 0
        self.started = False
        self.order = []

        asProperty(self, "cards", getter=lambda s: s._cards.values())

    def getMaxActPt(self, card):
        return self._cards[card].maxActPt

    def setMaxActPt(self, card, v):
        self._cards[card].maxActPt = v

    def getCurrActPt(self, card):
        return self._cards[card].currActPt

    def setCurrActPt(self, card, v):
        self._cards[card].currActPt = v

    def adjCurrActPt(self, card, v):
        self._cards[card].currActPt += v

    def getLoc(self, card):
        return self._cards[card].loc

    def adjLoc(self, card, loc):
        self._cards[card].loc += loc

    def setLoc(self, card, loc):
        self._cards[card].loc = loc

    def addPc(self, pc):
        self._cards[pc] = self.CardInfo(pc)

    def addBoss(self, boss):
        self.boss.append(boss)
        self._cards[boss] = self.CardInfo(boss, isBoss=True)

    def move(self, card, action, distance):
        self.adjLoc(card, distance)
        self.adjCurrActPt(card, -action)

    def __contains__(self, card):
        return card in self._cards

    def removePc(self, pc):
        if pc in self._cards:
            del self._cards[pc]
        if pc in self.order:
            self.order.remove(pc)

    def removeBoss(self, boss):
        self.boss.remove(boss)
        del self._cards[boss]

    def start(self):
        self.started = True
        self.lowestMOV = self.cards | e / map @ (lambda x: x.attrs["mov"]) | e / min
        keyfunc = lambda x: (x.attrs["dex"], x.attrs["luck"])
        g = lambda x: (lambda k, g: tuple(g))(*x)
        self.order = (
            list(self.cards)
            | e / (lambda x: sorted(x, key=keyfunc, reverse=True))
            | e ** {"key": keyfunc} / groupby
            | e / map @ g
            | e / list
        )

        for v in self.cards:
            v.maxActPt = v.attrs["mov"] - self.lowestMOV + 1
            v.currActPt = v.maxActPt

    def nextRound(self):
        for v in self.cards:
            v.currActPt = v.maxActPt
        self.round += 1

    def end(self):
        pass



def formOrderMap(s: ChaseScene) -> str:
    cards, orderLst = s._cards, s.order
    n = x.nickname
    info = cards[x.card]
    pt = info.currActPt
    maxPt = info.maxActPt
    f = lambda x: f"{n} ({pt}/{maxPt})"
    orderMap = " → ".join(", ".join(f(j) for j in i) for i in orderLst)

    return orderMap


def prepare(user, dbm, msg, args):
    db = dbm.get("trpg.chasing")
    db.set("@scene", ChaseScene())
    return ["* 追逐戰: 準備開始追逐戰"]


def start(user, dbm, msg, args):
    s = getScene(dbm)
    if s.started:
        return ["* 追逐戰已開始"]
    s.start()

    orderMap = formOrderMap(s)

    return ["* 追逐戰: 追逐戰開始!", f"* 行動順序為\n{orderMap}"]


def end(user, dbm, msg, args):
    s = getScene(dbm)
    s.end()

    db = dbm.get("trpg.chasing")
    db.set("@scene", None)
    return ["* 追逐戰: 追逐戰結束!"]


def nextRound(user, dbm, msg, args):
    s = getScene(dbm)
    s.nextRound()

    return ["* 追逐戰進入下一回合"]


def move(user, dbm, msg, args):
    s = getScene(dbm)

    if not s.started:
        return ["* 追逐戰未開始"]

    card = getCard(user)
    if card not in s:
        return ["* the card is not in the chase scene"]

    for eachOrder in s.order:
        if sum(i.currActPt for i in eachOrder) != 0:
            if card not in [i.card for i in eachOrder]:
                return [f"* 還沒到你的回合喔"]
            else:
                break

    consume = int(args["consume"])
    distance = int(args["distance"])

    if s._cards[card].currActPt < consume:
        return [f"* 行動點不足"]

    s.move(card, consume, distance)

    boss = s.boss[0]
    bossDistance = s._cards[card].loc - s._cards[boss].loc
    ret = [
        f"* {card.nickname} 消耗了 {consume} 行動點移動了 {distance} 的距離\n"
        + f"行動點尚餘 {s._cards[card].currActPt}, "
        + f"和 {boss.nickname} 的距離為 {bossDistance}"
    ]

    if sum(i.currActPt for i in s._cards.values()) == 0:
        ret.append(f"* 所有角色耗盡行動點, 可以進入下一個回合了")
    return ret


def join(user, dbm, msg, args):
    s = getScene(dbm)

    card = getCard(user)
    requiredAttrCheck(card, "mov", "dex", "luck")
    s.addPc(card)
    if args["loc"]:
        s.setLoc(card, int(args["loc"]))
    return [f"* 追逐戰: {card.nickname} 已加入追逐戰"]


def exitChasing(user, dbm, msg, args):
    s = getScene(dbm)

    card = getCard(user)
    s.removePc(card)
    return [f"* 追逐戰: {card.nickname} 已離開追逐戰"]


def joinboss(user, dbm, msg, args):
    s = getScene(dbm)

    card = getCard(user)
    requiredAttrCheck(card, "mov", "dex", "luck")

    s.addBoss(card)

    if args["loc"]:
        s.setLoc(card, int(args["loc"]))

    return [f"* 追逐戰: (boss) {card.nickname} 已加入追逐戰"]


def delboss(user, dbm, msg, args):
    s = getScene(dbm)

    card = getCard(user)
    s.removeBoss(card)
    return [f"* 追逐戰: (boss) {card.nickname} 已離開追逐戰"]


def chaseInfo(user, dbm, msg, args):
    s = getScene(dbm)

    if not s.started:
        return ["* 追逐戰未開始"]

    x = sorted(s.cards, key=lambda x: x.loc)
    locMap = f"{x[0].loc - 0}> {x[0].nickname}"
    for i, currCard in enumerate(x[:-1]):
        nextCard = x[i + 1]
        ncloc = nextCard.loc
        ccloc = currCard.loc
        ncname = nextCard.nickname

        locMap += f" {ncloc - ccloc}> {ncname}"

    orderMap = formOrderMap(s)

    return [f"* 行動順序: {orderMap}\n" f"* 位置: {locMap}"]


def init(api, handler, prefix=None, subfix=None):
    cmd = [
        (r"chase prepare", prepare),
        (r"chase start", start),
        (r"chase next", nextRound),
        (r"chase info", chaseInfo),
        (r"chase end", end),
        (r"joinboss\s?(?P<loc>\d*)", joinboss),
        (r"delboss", delboss),
        (r"join\s?(?P<loc>\d*)", join),
        (r"exit", exitChasing),
        (r"move (?P<consume>[+-]?\d+) (?P<distance>[+-]?\d+)", move),
    ]
    for i in cmd:
        handler.register(i[0], i[1], prefix=prefix, subfix=subfix)
