import json
import os
import re
import socket
import sys
import traceback

from stardust import POST
from typing import Callable, List, Dict, Tuple, Union

from stardust.pipe_fn import e

from stardust import flatten

from .data import Database, DatabaseManager, UserDAO, QQMessage, Reply

from multiprocessing import Process

class CommandHandler():
   def __init__(self, prefix: str=r"[\.．。]", subfix: str=""):
      self.prefix = prefix
      self.subfix = subfix
      self.cmdList = []


   def register(self, pattern: str, callback, prefix=None, subfix=None):
      print(f"*= register {pattern} to {callback.__name__}")
      prefix = prefix if prefix is not None else self.prefix
      subfix = subfix if subfix is not None else self.subfix
      self.cmdList.append({
            "full_pattern": re.compile(f"^{prefix}{pattern}{subfix}$", re.IGNORECASE|re.MULTILINE|re.DOTALL),
            "pattern": re.compile(pattern, re.IGNORECASE|re.MULTILINE|re.DOTALL),
            "callback": callback
         })


   def handle(self, msg: QQMessage):
      """Return list of callback"""
      # print(msg)

      dbm = DatabaseManager()

      todo = []

      moduleStatus = dbm.get("module.status")

      matchedFunc = []
      for x in self.cmdList:
         full_pattern, pattern, callback = x["full_pattern"], x["pattern"],  x["callback"]
         if not moduleStatus.get(callback.__name__, True):
            continue
         r = re.match(full_pattern, str(msg.content).strip())

         if (msg.msgType == "PrivateMessage" or msg.msgType == "TempMessage" or msg.at) and r is None:
            r = re.match(pattern, str(msg.content).strip())

         if r:
            matchedFunc.append(callback.__name__)
            todo.append((callback, r))

      if matchedFunc:
         print(f"Triggered {matchedFunc}")
      return todo


   def load(self, api, module, prefix: str=None, subfix: str=None):
      module.init(api, self, prefix, subfix)

class Pure():
   def __init__(self, func):
      self.func = func

   def __call__(self, *args, **kwargs):
      return self.func(*args, **kwargs)

   def __getattr__(self, name):
       return getattr(self.func, name)
