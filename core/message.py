import json
import os.path
import requests
import random

class MessageChain(list):
   def __str__(self):
      return "".join([str(i) for i in self])

   def __repr__(self):
      return str(self)

   def dict(self):
      return [i.dict() for i in self]

class Message():
   def __init__(self):
      self.type = None

   def dict(self):
      d = {
         "type": self.type,
      }
      for (k,v) in self.__dict__.items():
         if k == "type" or v is None:
            continue
         d[k] = v
      return d

   def __eq__(self, other):
      return isinstance(other, Message) and str(self) == str(other)

   def __repr__(self):
      k = ' '.join((k + '=' + v) for (k,v) in self.dict().items())
      return f"{self.type}[{k}]"

   def __str__(self):
      return str(self.dict())

class PlainText(Message):
   def __str__(self):
      return self.text

   def __init__(self, text):
      self.type = "Plain"
      self.text = text

class Image(Message):
   def __init__(self, path=None, imageId=None, url=None):
      self.type = "Image"
      self.path = path
      self.imageId = imageId
      if not path and not imageId and url:
         import config
         p = config.image_dest
         filename = '%032x' % random.getrandbits(128)
         while os.path.exists(os.path.join(p, filename)):
            filename = '%032x' % random.getrandbits(128)
         response = requests.get(url)
         if response.status_code == 200:
            with open(os.path.join(p, filename), "wb") as f:
               f.write(response.content)
            self.path = filename


   def __str__(self):
      return f"[mirai:image:{self.imageId or self.path}]"

class At(Message):
   def __init__(self, target):
      self.type = "At"
      self.target = int(target)

import json
class App(Message):
   def __init__(self, content):
      self.type = "App"
      self.content = json.loads(content)

   def dict(self):
      d = {
         "type": self.type,
         "content": json.dumps(self.content)
      }
      return d
