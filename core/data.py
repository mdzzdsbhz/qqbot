import json
import os
import os.path
from typing import List

class Database():

   def __init__(self, path: str):
      self.path = os.path.join("./data", path)
      if os.path.isfile(self.path):
         self.data = json.load(open(self.path, "r", encoding="utf-8"))
      else:
         self.data = {}
      self.refdata = {}

   def getFile(self, mode="r"):
      if "b" in mode:
         return open(self.path, mode)
      else:
         return open(self.path, mode, encoding="utf-8")


   def __contains__(self, key: str):
      if key.startswith("@"):
         d = self.refdata
      else:
         d = self.data
      return key in d


   def get(self, key: str, default=None):
      if key.startswith("@"):
         d = self.refdata
      else:
         d = self.data
      return default if key not in d else d[key]


   def set(self, key: str, value):
      # d = self.data
      if key.startswith("@"):
         d = self.refdata
         d[key] = value
      else:
         d = self.data
         d[key] = value
         s = json.dumps(self.data, ensure_ascii=False)
         with open(self.path, "wb+", buffering=0) as f:
            f.write(s.encode("utf-8"))


   def remove(self, key: str):
      if key.startswith("@"):
         d = self.refdata
      else:
         d = self.data
      if key not in d:
         return
      del d[key]

   def items(self):
      return self.data.items()

   def refitems(self):
      return self.refdata.items()


   def __getitem__(self, key: str):
      return self.get(key)


   def __setitem__(self, key: str, value):
      self.set(key, value)


class UserDAO():


   def __init__(self, uid: str, db: Database):
      assert uid is not None
      self.uid = uid
      self.db = db

      db.set("uid", uid)

   def getFile(self, mode="r"):
      return db.getFile(mode)


   def sendMsg(self, msg: List[str]):
      pass


   def get(self, key: str, default=None):
      return self.db.get(key, default)

   def getName(self):
      return self["nickname"] or self["usercard"] or self["username"]


   def set(self, key: str, value):
      self.db.set(key, value)


   def remove(self, key: str):
      self.db.remove(key)


   def __contains__(self, key: str):
      return key in self.db


   def __setitem__(self, key: str, value):
      self.set(key, value)


   def __getitem__(self, key: str):
      return self.get(key)


   def __delitem__(self, key: str):
      self.db.remove(key)


class DatabaseManager():
   __static = {}
   __staticUser = {}


   def get(self, name: str, path: str=None):
      if name not in self.__static:
         self.__static[name] = Database(path or f"Module.{name}.json")
      return self.__static[name]


   def isExist(self, name: str, path: str=None):
      return os.path.isfile(os.path.join("data/", (path or f"Module.{name}.json")))


   def getFile(self, name: str, path: str=None):
      if name not in self.__static:
         self.__static[name] = Database(path or f"Module.{name}.json")
      return self.__static[name].getFile()


   def getUser(self, uid: str):
      if uid not in self.__staticUser:
         db: Database = Database(f"User.{uid}.json")
         self.__staticUser[uid] = UserDAO(uid, db)

      return self.__staticUser[uid]


class QQMessage():
   def __init__(self, raw):
      # print(msg)
      def byDefault(dict, key, value, convert=lambda x: x):
         return convert(dict[key]) if key in dict and dict[key] is not None else value

      byDefOfRaw = lambda key: byDefault(raw, key, None, str)
      self.raw = raw
      self.time = byDefOfRaw('time')
      self.sender_id = byDefOfRaw('sender_id')
      self.sender = byDefOfRaw('sender')
      self.receiver_id = byDefOfRaw('receiver_id')
      self.receiver = byDefOfRaw('receiver')
      self.group_id = byDefOfRaw('group_id')
      self.group = byDefOfRaw('group')
      self.content = byDefault(raw, 'content', None)
      self.msgType = byDefOfRaw('msgType')
      self.at = byDefault(raw, "at", False)

   def __repr__(self):
      return repr(self.raw)


class Reply(BaseException):
   def __init__(self, msg=[]):
      self.msg = msg

class PrivateReply(Reply):
   def __init__(self, user_id, msg):
      super().__init__(msg)
      self.user_id = user_id

class GroupReply(Reply):
   def __init__(self, group_id, msg):
      super().__init__(msg)
      self.group_id = group_id
