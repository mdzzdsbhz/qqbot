from stardust import Bunch
import sched, time
import datetime
import requests
import tempfile
import pixiv
from core.data import DatabaseManager
from core import message
from core.main import Pure
from multiprocessing import Process

s = sched.scheduler(time.time, time.sleep)


def download_image():
    cookie = pixiv.cookie
    pList = []
    today = datetime.datetime.today()
    today = today.replace(hour=(today.hour // 12 * 12), minute=0, second=0)
    rangeStart = today - datetime.timedelta(days=1)
    rangeEnd = rangeStart + datetime.timedelta(hours=12)
    for page in range(1, 100):
        p = pixiv.get_newest_followed_illusts(cookie, page)
        for illust in p:
            d = datetime.datetime.strptime(illust["date"], "%Y/%m/%d/%H/%M/%S")
            print(rangeStart, d, rangeEnd)
            if rangeStart <= d < rangeEnd:
                info = pixiv.get_info(illust["pid"])
                ext = info["ext"]
                if info["xRestrict"] == 0 and info["bookmarkCount"] >= 50:
                    for page in range(0, int(illust["pageCount"])):
                        name = "{}_p{}.{}".format(illust["pid"], page, ext)
                        url = pixiv.img_url.format(
                            pid=illust["pid"], date=illust["date"], ext=ext, page=page
                        )
                        import config
                        path = config.image_dest + name
                        with open(path, "wb") as f:
                            print("Downloading {}".format(url))
                            response = requests.get(url, headers={"referer": url})
                            print("Response: {}".format(response.status_code))
                            if response.status_code != 200:
                               continue
                            f.write(response.content)
                            print("Sending: {}".format(name))
                            yield name
            elif d < rangeStart:
                return []


def push(user, dbm, msg, args):
    if args["id"] == "pixiv":
        m = dbm.get("push")
        pushTable = m.get("pixiv", [])
        pushTable.append(msg.group_id)
        m.set("pixiv", pushTable)
    return []

@Pure
def push_pixiv(pushTable, api, dbm):
    for path in download_image():
        for gid in pushTable:
            api.sendGroupMsg(gid, message.MessageChain([message.Image(path=path)]))
            pass


def start_push(api, dbm, push=True):
    if push:
        m = dbm.get("push")

        for key in ["pixiv"]:
            pushTable = m.get("pixiv", [])
            print(pushTable)
            if pushTable:
                f = eval("push_" + key)
                if isinstance(f, Pure):
                    Process(target=f, args=(pushTable, api, dbm)).start()
                else:
                    f(pushTable, api, dbm)

    t = datetime.datetime.today()
    next_push = datetime.datetime(
        t.year, t.month, t.day, (t.hour // 12) * 12
    ) + datetime.timedelta(hours=12)
    # print(next_push)
    # print(t)
    s.enterabs(time.mktime(next_push.timetuple()), 0, start_push, (api, dbm))


def init(api, handler, prefix=None, subfix=None):
    cmd = [(r"push\s(?P<id>.+)", push)]
    for i in cmd:
        handler.register(i[0], i[1], prefix=prefix, subfix=subfix)
    api.onstart = [lambda: start_push(api, DatabaseManager(), push=False)]
