from .osu_api import OsuApi, OsuUtil
import re
import json
from stardust import Bunch
import config

def osu_stat(user, dbm, data, args):
   osuApi = OsuApi(config.osu_apikey)
   stat = Bunch(osuApi.get_user(user=args["user"], mode=OsuUtil.modeId(args["mode"]))[0])
   return [
       f"{stat.username} Lv.{stat.level.split('.')[0]} ({stat.level.split('.')[1].ljust(4, '0')[:2]}%)\n"
       f"#{stat.pp_rank}\n"
       f"{('-' * 16)}\n"
       f"PP: {stat.pp_raw}\n"
       f"Ranked Score: {int(stat.ranked_score):,}\n"
       f"Acc: {float(stat.accuracy):.2f}%\n"
       f"PC: {stat.playcount}\n"
       f"Ranks: SS {int(stat.count_rank_ss) + int(stat.count_rank_ssh)} "
       f"S {int(stat.count_rank_s) + int(stat.count_rank_sh)} A {stat.count_rank_a}"
   ]


def osu_statme(user, dbm, data, args):
   osuid = user["osuid"]
   if not osuid:
      return ["* 請先設置你的osu!id (setid <osu!id>)"]
   return osu_stat(user, dbm, data,
                   re.match(r"(?P<user>.*)\s(?P<mode>.*)$", f"{osuid} {args.group('mode')}"))


def osu_setid(user, dbm, data, args):
   osuid = args.group('user')
   user['osuid'] = osuid
   nickname = user["nickname"]
   return [f"* {nickname} 的 osu! id 已綁定至 {osuid}"]


def osu_recent(user, dbm, data, args):
   osuid = user['osuid']
   if not osuid:
      return ["* 請先設置你的osu!id (setid osu!id)"]

   osuApi = OsuApi(config.osu_apikey)

   user_stat = osuApi.get_user(user=osuid)

   if len(user_stat) == 0:
      return [f"* 查無此id ({osuid})"]
   user_stat = user_stat[0]

   recent_stat = osuApi.get_user_recent(osuid, mode=OsuUtil.modeId(args.group('mode')))
   if len(recent_stat) == 0:
      return [f"* {osuid} 最近沒有遊玩記錄"]
   recent_stat = recent_stat[0]

   beatmap_id = recent_stat["beatmap_id"]
   beatmap_info = osuApi.get_beatmaps(beatmap_id=beatmap_id)[0]

   mods = OsuUtil.bitwise2mode(int(recent_stat['enabled_mods']))

   acc = OsuUtil.acc_calc(
      int(recent_stat['count300']), int(recent_stat['count100']), int(recent_stat['count50']),
      int(recent_stat['countmiss']))

   pp = OsuUtil.pp_calc(beatmap_id, int(recent_stat['maxcombo']),
                        OsuUtil.bitwise2mode(int(recent_stat['enabled_mods'])),
                        acc,
                        int(recent_stat['countmiss']))

   _cs = beatmap_info['diff_size']
   _ar = beatmap_info['diff_approach']
   _od = beatmap_info['diff_overall']
   _hp = beatmap_info['diff_drain']
   _length = int(beatmap_info["hit_length"])
   _bpm = beatmap_info['bpm']
   _stars = f"{float(beatmap_info['difficultyrating']):.2f}"

   if "HR" in mods or "EZ" in mods:
      _cs = f"{_cs}({float(pp['cs']):.2f})"
      _ar = f"{_ar}({float(pp['ar']):.2f})"
      _od = f"{_od}({float(pp['od']):.2f})"
      _hp = f"{_hp}({float(pp['hp']):.2f})"
      _stars = f"{_stars}({float(pp['stars']):.2f})"
   elif "DT" in mods or "HT" in mods:
      _ar = f"{_ar}({float(pp['ar']):.2f})"
      _od = f"{_od}({float(pp['od']):.2f})"
      _stars = f"{_stars}({float(pp['stars']):.2f})"

   if "DT" in mods or "HT" in mods:
      _bpm = f"{_bpm}({float(float(_bpm) * 1.5 if 'DT' in mods else 0.75):.2f})"
      _length = int(_length // (1.5 if 'DT' in mods else 0.75))

   _length = f"{_length // 60}:{_length % 60}"

   beatmap_info_str = \
      f"{beatmap_info['artist']} - {beatmap_info['title']}[{beatmap_info['version']}]\n" + \
      f"Beatmap by {beatmap_info['creator']}\n" + \
      f"Played by {osuid} on {recent_stat['date']}\n" + \
      f"CS:{_cs} AR:{_ar} OD:{_od} HP:{_hp}\n" + \
      f"BPM:{_bpm} Length: {_length} Stars:{_stars}\n"

   return [
       beatmap_info_str + "-" * 16 + "\n" +
       f"Score: {int(recent_stat['score']):,}\n" +
       f"300: {recent_stat['count300']}x 300g: {recent_stat['countgeki']}x\n" +
       f"100: {recent_stat['count100']}x 100k: {recent_stat['countkatu']}x\n" +
       f" 50: {recent_stat['count50']}x  miss: {recent_stat['countmiss']}x\n" +
       f"Combo: {recent_stat['maxcombo']}x/{pp['max_combo']}x Acc: {acc*100:.2f}%\n" +
       f"Rank: {recent_stat['rank']}\n" +
       f"Mods: {''.join(mods or ['None'])}\n" +
       f"PP: {pp['pp']:.2f} (aim: {pp['aim_pp']:.2f}, spd: {pp['speed_pp']:.2f}, acc: {pp['acc_pp']:.2f})\n"
       + "-" * 16 + "\n" + f"https://osu.ppy.sh/b/{beatmap_id}"]
   pass


def init(api, handler, prefix=None, subfix=None):
   cmd = [
      (r"stat\s(?P<user>.*?)\s?(?:\:(?P<mode>.*))?$", osu_stat),
      (r"statme\s?(?:\:(?P<mode>.*))?$", osu_statme),
      (r"setid\s?(?P<user>.+)$", osu_setid),
      (r"((?:4|5|t|f|d|e|r)(?:1|q|w|3|e|r|4|d)(?:x|d|f|v|c)(?:1|q|w|3|e|r|4)(?:b|h|j|m|n)(?:r|5|6|y|g|f|t))\s?(?:\:(?P<mode>.*))?$", osu_recent)
   ]
   for i in cmd:
      handler.register(i[0], i[1], prefix=prefix, subfix=subfix)
