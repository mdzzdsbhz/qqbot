import json
from stardust import *

get_user_url = "https://osu.ppy.sh/api/get_user"
get_user_recent_url = "https://osu.ppy.sh/api/get_user_recent"
get_beatmaps = "https://osu.ppy.sh/api/get_beatmaps"



class OsuUtil():
   MODE = {
      "std": 0,
      "taiko": 1,
      "ctb": 2,
      "mania": 3
   }
   MODS = {
      # "None"           : 0,
      "NoFail"         : 1,
      "Easy"           : 2,
      'TouchDevice'    : 4,
      'Hidden'         : 8,
      'HardRock'       : 16,
      'SuddenDeath'    : 32,
      'DoubleTime'     : 64,
      'Relax'          : 128,
      'HalfTime'       : 256,
      'Nightcore'      : 512, # Only set along with DoubleTime. i.e: NC only gives 576
      'Flashlight'     : 1024,
      'Autoplay'       : 2048,
      'SpunOut'        : 4096,
      'Relax2'         : 8192,	# Autopilot
      'Perfect'        : 16384, # Only set along with SuddenDeath. i.e: PF only gives 16416
      'Key4'           : 32768,
      'Key5'           : 65536,
      'Key6'           : 131072,
      'Key7'           : 262144,
      'Key8'           : 524288,
      'FadeIn'         : 1048576,
      'Random'         : 2097152,
      'Cinema'         : 4194304,
      'Target'         : 8388608,
      'Key9'           : 16777216,
      'KeyCoop'        : 33554432,
      'Key1'           : 67108864,
      'Key3'           : 134217728,
      'Key2'           : 268435456,
      'ScoreV2'        : 536870912,
      'LastMod'        : 1073741824,
   }
   MODSSHORTFORM = {
      1: "NF",
      2: "EZ",
      8: "HD",
      16: "HR",
      32: "SD",

      64: "DT",
      128: "RX",
      256: "HT",
      512: "NC",
      1024: "FL",

      2048: "AT",
      4096: "SO",
      8192: "AP",
      16384: "PF",
      536870912: "V2",
   }
   def __init__(self):
      pass

   @staticmethod
   def modeId(name):
      return OsuUtil.MODE[name] if name in OsuUtil.MODE else 0


   @staticmethod
   def pp_calc(beatmap_id, combo, mods, acc, countmiss):
      import subprocess, sys, os.path, os
      import platform

      if platform.system() == "Windows":
         cmd = ["PowerShell", "-ExecutionPolicy", "Bypass", "-NoProfile",
            f"{os.path.dirname(__file__)}/ppcalc.ps1", "-beatmap", str(beatmap_id),
            "-mods", f"\"{''.join(mods)}\"", "-combo", str(combo), '-miss', str(countmiss), '-acc', '{:.2f}%'.format(acc * 100)]
         result = subprocess.Popen(cmd, stdout=subprocess.PIPE)
         result, _ = result.communicate()
      else:
         beatmap_data = subprocess.Popen(["curl", f"https://osu.ppy.sh/osu/{beatmap_id}"], stdout=subprocess.PIPE)
         result = subprocess.check_output(["oppai", "-", "-ojson", f"+{''.join(mods)}", f"{combo}x", f"{countmiss}m", "{:.2f}%".format(acc*100)], stdin=beatmap_data.stdout)


      return json.loads(result)

   @staticmethod
   def acc_calc(s300, s100, s50, smiss):
      return (s300 * 300 + s100 * 100 + s50 * 50) / (300 * (s300 + s100 + s50 + smiss))

   @staticmethod
   def bitwise2mode(number):
      mods = []
      for k, v in OsuUtil.MODSSHORTFORM.items():
         if number & k == k:
            mods.append(v)
      if "NC" in mods:
         mods.remove("DT")
      if "PF" in mods:
         mods.remove("SD")
      return mods


class OsuApi():
   def __init__(self, apikey):
      self.apikey = apikey

   """
   Retrieve general user information.

   k - api key (required).
   u - specify a user_id or a username to return metadata from (required).
   m - mode (0 : osu!, 1 = Taiko, 2 = CtB, 3 = osu!mania). Optional, default value is 0.
   type - specify if u is a user_id or a username. Use string for usernames or id for user_ids. Optional, default behaviour is automatic recognition (may be problematic for usernames made up of digits only).
   event_days - Max number of days between now and last event date. Range of 1-31. Optional, default value is 1.
   """
   def get_user(self, user, mode=None, type=None, event_days=1):
      res = json.loads(POST(get_user_url, data=clearDictNone({
         "k": self.apikey,
         "u": user,
         "m": mode,
         "type": type,
         "event_days": event_days
      })).read())
      return res

   """
   Gets the user's ten most recent plays over the last 24 hours.

   k - api key (required).
   u - specify a user_id or a username to return recent plays from (required).
   m - mode (0 = osu!, 1 = Taiko, 2 = CtB, 3 = osu!mania). Optional, default value is 0.
   limit - amount of results (range between 1 and 50 - defaults to 10).
   type - specify if u is a user_id or a username. Use string for usernames or id for user_ids. Optional, default behavior is automatic recognition (may be problematic for usernames made up of digits only).
   """
   def get_user_recent(self, user, mode=None, type=None, limit=1):
      return json.loads(POST(get_user_recent_url, data=clearDictNone({
         "k": self.apikey,
         "u": user,
         "m": mode,
         "type": type,
         "limit": limit
      })).read())

   """
   Retrieve general beatmap information.

   k - api key (required).
   since - return all beatmaps ranked or loved since this date. Must be a MySQL date.
   s - specify a beatmapset_id to return metadata from.
   b - specify a beatmap_id to return metadata from.
   u - specify a user_id or a username to return metadata from.
   type - specify if u is a user_id or a username. Use string for usernames or id for user_ids. Optional, default behaviour is automatic recognition (may be problematic for usernames made up of digits only).
   m - mode (0 = osu!, 1 = Taiko, 2 = CtB, 3 = osu!mania). Optional, maps of all modes are returned by default.
   a - specify whether converted beatmaps are included (0 = not included, 1 = included). Only has an effect if m is chosen and not 0. Converted maps show their converted difficulty rating. Optional, default is 0.
   h - the beatmap hash. It can be used, for instance, if you're trying to get what beatmap has a replay played in, as .osr replays only provide beatmap hashes (example of hash: a5b99395a42bd55bc5eb1d2411cbdf8b). Optional, by default all beatmaps are returned independently from the hash.
   limit - amount of results. Optional, default and maximum are 500.
   """
   def get_beatmaps(self, beatmapset_id=None, beatmap_id=None, user=None, mode=None, a=None, hash=None, limit=1, type=None):
      data = {
         "k": self.apikey,
         "b": beatmap_id,
         "s": beatmapset_id,
         "u": user,
         "m": mode,
         "type": type,
         "a": a,
         "limit": limit,
         "h": hash
      }
      res = json.loads(POST(get_beatmaps, data=clearDictNone(data)).read())
      return res
