param(
   [string] $beatmap,
   [string] $combo,
   [string] $miss,
   [string] $mods,
   [string] $acc
)
(New-Object System.Net.WebClient).DownloadString("https://osu.ppy.sh/osu/$beatmap") | oppai - -ojson +$mods ${combo}x ${miss}m ${acc}%
