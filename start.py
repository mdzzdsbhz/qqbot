from core.main import Database, CommandHandler, Pure
import signal
import sys
import re
from numpy import random
from stardust import POST
from stardust.pipe_fn import e
from stardust.functional import take
from core import message

sys.path.append(".")


def nn(user, dbm, data, args):
    old = user["nickname"] or user["usercard"]
    if args["name"] == "":
        del user["nickname"]
        return [f"* {old} 取消了暱稱"]
    else:
        user["nickname"] = args["name"]
        nickname = user["nickname"]
        return [f"* {old} 的新暱稱是 {nickname}"]


def enable(user, dbm, data, args):
    db = dbm.get("module.status")
    print(f"{data.sender_id} try to enable {args[1]}")
    db.set(args[1], True)
    return [f"* enabled {args[1]}"]


def disable(user, dbm, data, args):
    db = dbm.get("module.status")
    print(f"{data.sender_id} try to disable {args[1]}")
    db.set(args[1], False)
    return [f"* disabled {args[1]}"]


def helpMsg(user, dbm, data, args) -> list:
    msg = []
    return msg


def archiveHistory(user, dbm, data, args) -> list:
    import gzip

    db: Database = dbm.get("msg")

    import gzip
    import shutil
    import time

    with db.getFile("rb") as f_in:
        with gzip.open(db.path + f".{int(time.time())}.gz", "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)

    db.set("history", [])
    return []


def getHistory(user, dbm, data, args) -> list:
    db = dbm.get("msg")
    history = db.get("history")
    if not history:
        return []
    else:
        if not args[2]:
            offset = int(args[1] or 10)
            msgLen = int(args[1] or 10)
        else:
            offset = int(args[1] or 10)
            msgLen = int(args[2] or 10)

        if msgLen == 0:
            return []
        elif msgLen > 0:
            lst = db.get("history")[offset - msgLen : offset]
        else:
            lst = db.get("history")[max(offset, 0) - msgLen : max(offset, 0)]

        lst.reverse()

        def f(x):
            t, sender_id, msg = x
            n = dbm.getUser(str(sender_id))["usercard"] or ("u" + str(sender_id))
            d = re.sub(r"\[CQ:([^,]+).*\]", r"[\1]", msg)
            return f"{n}: {d}"

        return ["\n".join(f(i) for i in lst)]


def recordMsg(user, dbm, data, args) -> list:
    import time

    db = dbm.get("msg")
    db.set("history", db.get("history", []))
    # print(data.time, time.time())
    db.get("history").insert(0, (data.time, data.sender_id, data.content))
    db.set("history", db.get("history", []))
    return []


def dynCommand(user, dbm, data, args) -> list:
    db = dbm.get("dyn.msg")
    lst = db.get("patt.lst", [])
    ret = []

    for i in lst:
        patt, rep = i
        res = re.search(patt, args["msg"], re.IGNORECASE | re.MULTILINE | re.DOTALL)
        if res:

            def repl(o):
                try:
                    return res.group(int(o.group(1)))
                except IndexError:
                    return res.group(o.group(1))

            ret.append(
                list(rep)
                | e / map @ (lambda x: re.sub(r"(?!\\)\{(.*)(?!\\)\}", repl, x))
            )
    return ret[random.randint(len(ret))] if ret else ret


def regDynCommand(user, dbm, data, args) -> list:
    db = dbm.get("dyn.msg")
    lst = db.get("patt.lst", [])
    lst.append((args["patt"], args["reply"].split(" ")))
    db.set("patt.lst", lst)
    return ["* 已登錄"]


def autoreply(user, dbm, data, args) -> list:
    db = dbm.get("dyn.msg")
    lst = db.get("autoreply.lst", [])
    ret = []
    for i in lst:
        patt, rep = i
        if re.match(patt, args["msg"], re.IGNORECASE | re.MULTILINE | re.DOTALL):
            ret.append(rep)
    return ret[random.randint(len(ret))] if ret else ret


def repeater(user, dbm, data, args):
    import datetime
    import time

    # msg = args["msg"]
    msg = data.content

    if not hasattr(repeater, "lastMsg") or repeater.lastMsg != msg:
        repeater.lastMsg = msg
        repeater.lastMsgSender = data.sender_id
        return []

    if repeater.lastMsg == msg and repeater.lastMsgSender == data.sender_id:
        return []

    lastMsg = repeater.lastMsg
    repeater.lastMsg = msg

    now = datetime.datetime.now()

    def f():
        return [GroupReply(data.group_id, msg)] if random.randint(50) != 0 else ["(打斷複讀!)"]

    if not hasattr(repeater, "lastRepeatedMsg"):
        repeater.lastRepeatedMsg = (now.day, now.hour), msg
        return f()

    lastRepeatedMsg = repeater.lastRepeatedMsg
    if msg == lastRepeatedMsg[1] and (now.day, now.hour) > lastRepeatedMsg[0]:
        repeater.lastRepeatedMsg = (now.day, now.hour), msg
        return f()

    if msg != lastRepeatedMsg[1]:
        repeater.lastRepeatedMsg = (now.day, now.hour), msg
        return f()

    return []


def banu(user, dbm, data, args):
    day = int((args["day"] or "0d")[:-1])
    hr = int((args["hr"] or "0h")[:-1])
    minute = int((args["min"] or "0m")[:-1])

    POST(
        "http://127.0.0.1:5700/set_group_ban",
        data={
            "user_id": data.sender_id,
            "group_id": data.group_id,
            "duration": min(
                max(60, day * 24 * 60 * 60 + hr * 60 * 60 + minute * 60), 2591940
            ),
        },
    )

    return []


def unbanu(user, dbm, data, args):
    POST(
        "http://127.0.0.1:5700/set_group_ban",
        data={"user_id": args["id"], "group_id": data.group_id, "duration": 0,},
    )

    return []


from google import google


def googleit(user, dbm, data, args):
    return (
        google.search(args["query"])
        | e
        / map
        @ (lambda x: str(x.name[:18]) + "...: " + str(x.link or x.google_link))
        | e / take @ 5
        | e / "\n--------\n".join
        | e / (lambda x: [x])
    )


import math
from core.data import QQMessage, Reply, PrivateReply, GroupReply


def rj(user, dbm, data, args):
    num = int(args["num"])
    num2 = math.ceil(num / 1000) * 1000
    num = str(num).zfill(6)
    num2 = str(num2).zfill(6)
    return [
        Reply(
            message.MessageChain(
                [
                    message.Image(
                        url=f"https://img.dlsite.jp/modpub/images2/work/doujin/RJ{num2}/RJ{num}_img_main.jpg"
                    )
                ]
            ),
        )
    ]

def bj(user, dbm, data, args):
    num = int(args["num"])
    num2 = math.ceil(num / 1000) * 1000
    num = str(num).zfill(6)
    num2 = str(num2).zfill(6)
    return [
        Reply(
            message.MessageChain(
                [
                    message.Image(
                        url=f"https://img.dlsite.jp/modpub/images2/work/books/BJ{num2}/BJ{num}_img_main.jpg"
                    )
                ]
            ),
        )
    ]

def vj(user, dbm, data, args):
    num = int(args["num"])
    num2 = math.ceil(num / 1000) * 1000
    num = str(num).zfill(6)
    num2 = str(num2).zfill(6)
    return [
        Reply(
            message.MessageChain(
                [
                    message.Image(
                        url=f"https://img.dlsite.jp/modpub/images2/work/professional/VJ{num2}/VJ{num}_img_main.jpg"
                    )
                ]
            ),
        )
    ]


def cmd_pixiv(user, dbm, data, args):
    import pixiv
    pid = int(args["pid"])
    info = pixiv.get_info(pid)
    name = info["id"] + "." + info["ext"]
    import config
    path = config.image_dest + name
    pixiv.download_image(info["url"], path)
    return [Reply(message.MessageChain([message.Image(path=name)]))]

@Pure
def cmd_setu(user, dbm, data, args):
    import asyncio
    import requests
    import pixiv
    import config
    res = requests.get("https://api.lolicon.app/setu/", params={
        "apikey": config.setu_lolicon_apikey,
        "num": int(args["num"])
    }).json()

    loop = asyncio.get_event_loop()
    async def eachLoop(p):
        name = f"{p['pid']}_p{p['p']}"
        import config
        path = config.image_dest + name
        url = p["url"]
        pixiv.download_image(url, path)
        return Reply(message.MessageChain([message.Image(path=name)]))
    tasks = []
    for p in res["data"]:
       tasks.append(eachLoop(p))

    results = loop.run_until_complete(asyncio.gather(*tasks))

    return results

def cmd_b23tv(user, dba, data, args):
    app_content = None
    for k in data.content:
        if isinstance(k, message.App):
            app_content = k
    if app_content is None:
        return []
    if int(app_content.content["meta"]["detail_1"]["appid"]) == 1109937557:
        return [
            f"[哔哩哔哩]\n{app_content.content['meta']['detail_1']['desc']}\n"+
            f"{app_content.content['meta']['detail_1']['qqdocurl'].split('?')[0]}"
        ]
    else:
        return []

def main(test=False):
    # /pm <set> <group_id|*|~> <
    prefix = r"(?:[\.。!！:：~～/／\-—])"
    handler = CommandHandler(prefix=prefix)

    from miraiqq import MiRaiApi
    import config

    msgReceiver = MiRaiApi(config.mirai_url, config.mirai_qqnum, handler)

    cmd = [
        (r"nn\s?(?P<name>.*)", nn),
        (r"enable\s?(.*)", enable),
        (r"disable\s?(.*)", disable),
        (r"help\s?(.*)?", helpMsg),
        (r"history\s?(\d*)?(?:[：:]([+-]?\d*))?", getHistory),
        (r"cleanhistory", archiveHistory),
        (r"sleep\s(?P<day>\d+d)?(?P<hr>\d+h)?(?P<min>\d+m)?", banu),
        (r"unban\s(?P<id>\d+)", unbanu),
        (r"(?P<msg>.*)", dynCommand),
        (r"reg\s(?P<patt>[^\s]+)\s(?P<reply>.*)", regDynCommand),
        (r"google\s(?P<query>.+)", googleit),
        (r"rj(?P<num>\d+)", rj),
        (r"bj(?P<num>\d+)", bj),
        (r"vj(?P<num>\d+)", vj),
        (r"pixiv\s(?P<pid>\d+)", cmd_pixiv),
        (r"setu\s(?P<num>\d+)", cmd_setu),
    ]
    for i in cmd:
        handler.register(i[0], i[1])

    cmd = [
        # (r"(.*)", recordMsg),
        (r"(?P<msg>.*)", repeater),
        (r"(?P<msg>.*)", autoreply),
        (r".*", cmd_b23tv),
    ]
    for i in cmd:
        handler.register(i[0], i[1], prefix="", subfix="")

    plugins = ["osu.main", "dice.main", "schedtask.main"]

    for name in plugins:
        __import__(name)
        module = sys.modules[name]
        handler.load(msgReceiver, module, prefix=prefix)

    __import__("dice.action")
    handler.load(msgReceiver, sys.modules["dice.action"], prefix="#")

    msgReceiver.start()


if __name__ == "__main__":
    main(False)


def signal_handler(signal, frame):
    sys.exit(0)


signal.signal(signal.SIGINT, signal_handler)
